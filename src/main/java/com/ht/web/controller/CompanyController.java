package com.ht.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ht.web.controller.vo.CompanyVo;
import com.ht.web.entity.Company;
import com.ht.web.service.CompanyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;
    
	@RequestMapping(value= "/add", method = RequestMethod.POST)
	public String add(CompanyVo vo) {
	    try {
	        companyService.saveOrUpdate(vo);
	        
	        return String.format("add %s company ok.", vo.getName());
	    } catch (Exception e) {
	        log.error(e.getMessage(), e);
	    }
	    
	    return "add company fail.";
	}
	
   @RequestMapping(value= "/update", method = RequestMethod.POST)
    public String update(CompanyVo vo) {
        try {
            if (vo.getId() != null) {
                companyService.saveOrUpdate(vo);
                
                return String.format("add %s company ok.", vo.getName());
            } 
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        
        return "update company fail.";
    }
	
	@RequestMapping(value= "/delete")
    public String delete(Integer id) {
        try {
            companyService.delete(id);
            
            return "delete company ok.";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        
        return "delete company fail.";
    }
	
	@RequestMapping(value= "/list")
    public String list(CompanyVo vo) {
	    List<Company> list = companyService.findAll();
	    
        return list.toString();
    }
	

}
