package com.ht.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ht.web.controller.vo.ClientVo;
import com.ht.web.entity.Client;
import com.ht.web.service.ClientService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/client")
@RestController
public class ClientController {
    
    @Autowired
    private ClientService clientService;
    
    @RequestMapping(value= "/add", method = RequestMethod.POST)
    public String add(ClientVo vo) {
        try {
            Client client = clientService.saveOrUpdate(vo);
            
            return String.format("add %s belong %s ok.", client.getName(), client.getCompany().getName());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        
        return "add client fail.";
    }
    
    @RequestMapping(value= "/multi/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String multiAdd(@RequestBody List<ClientVo> clients) {
        try {
            clientService.multiSave(clients);
            
            return "add multi client ok.";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        
        return "add multi client fail.";
    }
    
   @RequestMapping(value= "/update", method = RequestMethod.POST)
    public String update(ClientVo vo) {
        try {
            if (vo.getId() != null) {
                Client client = clientService.saveOrUpdate(vo);
                
                return String.format("update %s belong %s ok.", client.getName(), client.getCompany().getName());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        
        return "update client fail.";
    }
    
    @RequestMapping(value= "/delete")
    public String delete(Integer id) {
        try {
            clientService.delete(id);
            
            return "delete client ok.";
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            
        }
        
        return "delete client fail.";
    }
    
    @RequestMapping(value= "/list")
    public String list(ClientVo vo) {
        List<Client> list = clientService.findAll();
        
        
        return list.toString();
    }
}
