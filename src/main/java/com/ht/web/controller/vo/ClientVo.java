package com.ht.web.controller.vo;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class ClientVo {

    private Integer id;
    private Integer companyId;
    private String name;
    private String email;
    private String phone;
    private CompanyVo companyVo;
    
}
