package com.ht.web.controller.vo;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class CompanyVo {

    private Integer id;
    private String name;
    private String address;

    
}
