package com.ht.web.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.ht.web.entity.Company;

@Component
public interface CompanyRepositoryIntf extends JpaRepository<Company, Integer>, Serializable {

}
