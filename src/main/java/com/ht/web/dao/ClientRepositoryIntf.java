package com.ht.web.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ht.web.entity.Client;

@Repository
public interface ClientRepositoryIntf extends JpaRepository<Client, Integer>, Serializable {
    

}
