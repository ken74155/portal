package com.ht.web.enums;

import lombok.Getter;

/**
 * 資料狀態.
 */
public enum Activation {

    ACTIVE("啟用"),
    INACTIVE("停用");

    @Getter
    private String label;
    
    Activation(String label) {
        this.label = label;
    }

}
