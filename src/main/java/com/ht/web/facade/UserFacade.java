package com.ht.web.facade;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.base.Optional;

/**
 * 
 * @author aken_kao
 *
 */
public class UserFacade {
    
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * 確認 是否已經登入
     *
     * @return 是否登入
     */
    public static boolean isAuthenticated() {
        Authentication authentication = getAuthentication();
        
        if (authentication == null || !authentication.isAuthenticated()) {
            return false;
        }
        if (!authentication.isAuthenticated() || authentication.getName() == null
                || authentication.getName().equalsIgnoreCase("anonymousUser")) {
            return false;
        }
        return true;
    }
    
    /**
     * 取得 CrowdUserDetail
     *
     * @return CrowdUserDetail
     */
    public static Optional<UserDetails> getUserDetail() {
        Authentication authentication = getAuthentication();
        if (authentication != null && authentication.isAuthenticated()
                && authentication.getPrincipal() instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            return Optional.of((UserDetails) userDetails);
        }
        return Optional.absent();
    }
    
    /**
     * 取得登入的UserId
     *
     * @return userId
     */
    public static String getUserId() {
        if (getUserDetail().isPresent()) {
            UserDetails details = getUserDetail().get();
            return details.getUsername();
        }
        return "";
    }
}
