package com.ht.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.ht.web.security.handler.CustomAccessDeniedHandler;

@ComponentScan("com.ht.web.security")
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication()
            .withUser("root").password("{noop}1234").roles("ROOT")
            .and()
            .withUser("admin").password("{noop}1234").roles("ADMIN")
            .and()
            .withUser("operator").password("{noop}1234").roles("OPERATOR");
            
    }
    
    /*
     * configure setting is for normal sub-project like system-web , form-projects , not for portal.
     * portal login need to write another setting
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/**/add").hasAnyRole("ROOT", "OPERATOR")
            .antMatchers("/**/delete").hasAnyRole("ROOT", "ADMIN")
            .antMatchers("/**/update").hasAnyRole("ROOT", "ADMIN")
            .antMatchers("/**/list").hasAnyRole("ROOT", "OPERATOR", "ADMIN")
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
            .and()
            .headers().frameOptions().disable()
            .and()
            .csrf().disable()
            .httpBasic();
        
    }
    
    

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        // All of Spring Security will ignore the requests
        webSecurity.ignoring()
                .antMatchers("/resources/**")
                .antMatchers("/css/**")
                .antMatchers("/img/**")
                .antMatchers("/images/**")
                .antMatchers("/js/**")
                .antMatchers("/template/**")
                .antMatchers("/error/**")
                .antMatchers("/cache/**")
                .antMatchers("/fonts/**")
                .antMatchers("/webresource/**");
    }

    
}
