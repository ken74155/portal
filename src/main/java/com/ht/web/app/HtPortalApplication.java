package com.ht.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.ht.web.config.SecurityConfig;

@SpringBootApplication
@ComponentScan(value = {"com.ht.web"})
@Import(SecurityConfig.class)
public class HtPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(HtPortalApplication.class, args);
	}

}
