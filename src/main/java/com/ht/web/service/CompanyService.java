package com.ht.web.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ht.web.controller.vo.CompanyVo;
import com.ht.web.dao.CompanyRepositoryIntf;
import com.ht.web.entity.Company;
import com.ht.web.facade.UserFacade;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepositoryIntf companyRepository;
    
    @Transactional
    public Company saveOrUpdate(CompanyVo vo) {
        Date currentDate = new Date();
        Company entity = null;
        if(vo.getId() == null) {
            entity = new Company();
            entity.setCreatedAt(currentDate);
            entity.setCreatedBy(UserFacade.getUserId());
        } else {
            entity = companyRepository.findById(vo.getId()).get();
        }
        entity.setName(vo.getName());
        entity.setAddress(vo.getAddress());
        entity.setUpdatedAt(currentDate);
        entity.setUpdatedBy(UserFacade.getUserId());
        
        return companyRepository.save(entity);
    }
    
    @Transactional
    public void delete(Integer id) {
        companyRepository.deleteById(id);
        
    }
    
    public List<Company> search(CompanyVo vo) {
        return findAll();
        
    }
    
    public List<Company> findAll() {
        return companyRepository.findAll();
        
    }
    
}
