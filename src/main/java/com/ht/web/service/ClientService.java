package com.ht.web.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ht.web.controller.vo.ClientVo;
import com.ht.web.controller.vo.CompanyVo;
import com.ht.web.dao.ClientRepositoryIntf;
import com.ht.web.dao.CompanyRepositoryIntf;
import com.ht.web.entity.Client;
import com.ht.web.entity.Company;
import com.ht.web.facade.UserFacade;

@Service
public class ClientService {

    @Autowired
    private ClientRepositoryIntf clientRepository;
    @Autowired
    private CompanyRepositoryIntf companyRepository;
    
    @Transactional
    public Client saveOrUpdate(ClientVo vo) {
        Date currentDate = new Date();
        Client entity = null;

        Company company = companyRepository.findById(vo.getCompanyId()).get();
        if(vo.getId() == null) {
            entity = new Client();
            entity.setEmail(vo.getEmail());
            entity.setCreatedAt(currentDate);
            entity.setCreatedBy(UserFacade.getUserId());
        } else {
            entity = clientRepository.findById(vo.getId()).get();
        }
        
        entity.setCompany(company);
        entity.setName(vo.getName());
        entity.setEmail(vo.getEmail());
        entity.setPhone(vo.getPhone());

        entity.setUpdatedAt(currentDate);
        entity.setUpdatedBy(UserFacade.getUserId());
        
        return clientRepository.save(entity);
    }
    
    @Transactional
    public void multiSave(List<ClientVo> clients) {
        for (ClientVo vo : clients) {
            saveOrUpdate(vo);
        }
    }
    
    
    @Transactional
    public void delete(Integer id) {
        clientRepository.deleteById(id);
        
    }
    
    public List<Client> search(CompanyVo vo) {
        return findAll();
        
    }
    
    public List<Client> findAll() {
        return clientRepository.findAll();
        
    }
}
