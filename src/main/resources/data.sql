CREATE TABLE company (
  id            INT AUTO_INCREMENT  PRIMARY KEY,
  name          VARCHAR(250) NOT NULL,
  address       VARCHAR(250),
  created_by    VARCHAR(250) NOT NULL,
  created_at    TIMESTAMP    NOT NULL,
  updated_by    VARCHAR(250),
  updated_at    TIMESTAMP
);

CREATE TABLE client (
  id            INT AUTO_INCREMENT  PRIMARY KEY,
  company_id    VARCHAR(250) NOT NULL,
  name          VARCHAR(250) NOT NULL,
  email         VARCHAR(250),
  phone         VARCHAR(250),
  created_by    VARCHAR(250) NOT NULL,
  created_at    TIMESTAMP    NOT NULL,
  updated_by    VARCHAR(250),
  updated_at    TIMESTAMP
);

INSERT INTO company (name, address, created_by, created_at) VALUES ('天脈', '台北市中山區', 'root', now());
INSERT INTO company (name, address, created_by, created_at) VALUES ('中佑', '台中市西屯區', 'root', now());

INSERT INTO client (company_id, name, created_by, created_at) VALUES ((select id from company where name = '天脈'), 'kai_chang', 'root', now());
INSERT INTO client (company_id, name, created_by, created_at) VALUES ((select id from company where name = '中佑'), 'aken_kao', 'root', now());
