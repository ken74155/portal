1.共3位使用者, 帳密如下
	root/1234		權限:新增 修改 刪除 查詢
	admin/1234		權限:     修改 刪除 查詢
	operator/1234   權限:新增           查詢

2.CRUD APIs如下
	Company
	http://localhost:8081/portal/company/add
	http://localhost:8081/portal/company/update
	http://localhost:8081/portal/company/delete
	http://localhost:8081/portal/company/list

	Client
	http://localhost:8081/portal/client/add
	http://localhost:8081/portal/client/multi/add
	http://localhost:8081/portal/client/update
	http://localhost:8081/portal/client/delete
	http://localhost:8081/portal/client/list
	
	
3.H2 DB資訊如下
	GUI: http://localhost:8081/portal/h2-console
	driver class:org.h2.Driver
	JDBC URL:jdbc:h2:mem:testdb
	username:sa
	password:1234
	
4.驗證方式為Http Basic Auth
  測試時於Postman加入上述使用者
  
  
5. add multi client
	json格式如下, postman contenType需改為json
  [
   {
      "companyId":1,
      "name":"kao",
      "email":"kao@bb.com",
      "phone":"3345678"
   },
   {
      "companyId":1,
      "name":"kao2",
      "email":"kao2@bb.com",
      "phone":"3345678"
   }
]